package com.kittinan.mobileguideapplication.presentation.commons.string

object StringSourceHolder {

    lateinit var stringSource: StringSource

    fun getString(stringId: Int) = stringSource.getString(stringId)
}