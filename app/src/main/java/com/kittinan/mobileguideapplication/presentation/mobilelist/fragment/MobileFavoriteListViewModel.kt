package com.kittinan.mobileguideapplication.presentation.mobilelist.fragment

import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.presentation.commons.ListType
import com.kittinan.mobileguideapplication.presentation.mobilelist.listener.OnMobileItemChangeListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MobileFavoriteListViewModel(
        mobileInteractor: MobileInteractor,
        itemChangeListener: OnMobileItemChangeListener? = null,
        itemClickListener: (Int) -> Unit
) : MobileListBaseViewModel(mobileInteractor, itemChangeListener, itemClickListener) {

    override var listType = ListType.FAVORITE

    init {
        updateDataWithSelectedSortType()
    }

    override fun updateDataWithSelectedSortType() {
        compositeDisposable.add(
                mobileInteractor.getFavorites()
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe {
                            items.clear()
                            handleViewOnSubscribe()
                        }
                        .doAfterTerminate { handleViewAfterTerminate() }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { mobiles ->
                                    val itemViewModels = mobiles.map { mapToMobileItemViewModel(it) }
                                    items.addAll(getSortedItems(itemViewModels, sortType))
                                },
                                {
                                    handleViewOnError()
                                    handleError(it)
                                }
                        )
        )
    }
}