package com.kittinan.mobileguideapplication.presentation.commons.string

import android.content.res.Resources

class ResourceStringSource(private val resource: Resources): StringSource {

    override fun getString(stringId: Int): String = resource.getString(stringId)
}