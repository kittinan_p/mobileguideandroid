package com.kittinan.mobileguideapplication.presentation.commons.adapters

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("imageUrl", "imagePlaceHolder")
fun setImageUrl(view: ImageView, imageUrl: String?, imagePlaceHolder: Drawable?) {
    if (imageUrl == null || imageUrl.isBlank()) {
        Glide.with(view)
                .clear(view)
        view.setImageDrawable(imagePlaceHolder)
    } else {
        Glide.with(view)
                .load(imageUrl)
                .apply(RequestOptions()
                        .fitCenter()
                        .placeholder(imagePlaceHolder)
                        .error(imagePlaceHolder)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                )
                .into(view)
    }
}

@BindingAdapter("visible")
fun setVisible(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}