package com.kittinan.mobileguideapplication.presentation.commons.string

class MockStringSource : StringSource {

    override fun getString(stringId: Int): String = "string id: $stringId"
}