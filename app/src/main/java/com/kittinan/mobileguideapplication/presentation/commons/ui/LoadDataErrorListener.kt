package com.kittinan.mobileguideapplication.presentation.commons.ui

interface LoadDataErrorListener {
    fun onRetry()
}