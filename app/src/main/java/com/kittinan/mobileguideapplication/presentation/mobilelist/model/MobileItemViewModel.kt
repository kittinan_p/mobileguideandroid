package com.kittinan.mobileguideapplication.presentation.mobilelist.model

import android.databinding.Bindable
import com.kittinan.mobileguideapplication.BR
import com.kittinan.mobileguideapplication.R
import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.presentation.BaseViewModel
import com.kittinan.mobileguideapplication.presentation.commons.ListType
import com.kittinan.mobileguideapplication.presentation.commons.ToggleFavorite
import com.kittinan.mobileguideapplication.presentation.commons.string.StringSourceHolder
import com.kittinan.mobileguideapplication.presentation.mobilelist.listener.OnMobileItemChangeListener
import io.reactivex.rxkotlin.subscribeBy

class MobileItemViewModel(
        val mobileInteractor: MobileInteractor,
        val mobile: Mobile,
        val listType: ListType? = null,
        private val itemChangeListener: OnMobileItemChangeListener? = null,
        private val itemClickListener: ((Int) -> Unit)? = null
) : BaseViewModel() {

    val brand = mobile.brand
    val title = mobile.name
    val description = mobile.description
    val imageUrl = mobile.thumbImageURL
    val rating = StringSourceHolder.getString(R.string.mobile_rating_text).replace("%NUM%", "${mobile.rating}")
    val price = StringSourceHolder.getString(R.string.mobile_price_text).replace("%NUM%", "${mobile.price}")

    @get:Bindable
    var favorite = mobile.favorite ?: false
        set(value) {
            field = value
            mobile.favorite = value
            notifyPropertyChanged(BR.favorite)
        }

    fun toggleFavorite() {
        favorite = !favorite
        mobileInteractor
                .updateFavorite(mobile)
                .subscribeBy(
                        onComplete = {
                            val toggleFavorite = if (favorite) ToggleFavorite.ADD else ToggleFavorite.REMOVE
                            itemChangeListener?.onToggleFavorite(toggleFavorite, listType)
                        },
                        onError = {
                            handleError(it)
                        }
                )
    }

    fun onItemClick() {
        itemClickListener?.invoke(mobile.id)
    }
}