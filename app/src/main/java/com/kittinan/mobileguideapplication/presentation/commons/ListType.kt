package com.kittinan.mobileguideapplication.presentation.commons

enum class ListType {
    DEFAULT,
    FAVORITE
}