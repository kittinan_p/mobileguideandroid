package com.kittinan.mobileguideapplication.presentation.mobilelist

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.kittinan.mobileguideapplication.R
import com.kittinan.mobileguideapplication.databinding.ActivityMobileBinding
import com.kittinan.mobileguideapplication.presentation.BaseActivity
import com.kittinan.mobileguideapplication.presentation.commons.ListType
import com.kittinan.mobileguideapplication.presentation.commons.SortType
import com.kittinan.mobileguideapplication.presentation.commons.ToggleFavorite
import com.kittinan.mobileguideapplication.presentation.mobilelist.adapter.MobilePagerAdapter
import com.kittinan.mobileguideapplication.presentation.mobilelist.listener.OnMobileItemChangeListener

class MobileActivity : BaseActivity<MobileViewModel>(), OnMobileItemChangeListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMobileBinding>(this, R.layout.activity_mobile)
    }

    private lateinit var adapter: MobilePagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        initializeTabs()
    }

    override fun onCreateViewModel() = MobileViewModel()

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_mobile_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.isChecked = item.isChecked.not()
        return when (item.itemId) {
            R.id.menu_sort_price_asc -> {
                adapter.sortAllTabs(SortType.PRICE_ASC)
                true
            }
            R.id.menu_sort_price_desc -> {
                adapter.sortAllTabs(SortType.PRICE_DESC)
                true
            }
            R.id.menu_sort_rating_desc -> {
                adapter.sortAllTabs(SortType.RATING_DESC)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onToggleFavorite(toggleFavorite: ToggleFavorite, listType: ListType?) {
        adapter.onToggleFavorite(toggleFavorite, listType)
    }

    private fun initializeTabs() {
        adapter = MobilePagerAdapter(this)
        binding.viewPager.adapter = adapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }
}