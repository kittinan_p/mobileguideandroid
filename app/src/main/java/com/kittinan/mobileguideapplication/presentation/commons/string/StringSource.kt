package com.kittinan.mobileguideapplication.presentation.commons.string

interface StringSource {
    fun getString(stringId: Int): String
}