package com.kittinan.mobileguideapplication.presentation.mobilelist.fragment

import android.databinding.Bindable
import android.databinding.ObservableArrayList
import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.presentation.BaseViewModel
import com.kittinan.mobileguideapplication.presentation.commons.ListType
import com.kittinan.mobileguideapplication.presentation.commons.SortType
import com.kittinan.mobileguideapplication.presentation.commons.ui.LoadDataErrorListener
import com.kittinan.mobileguideapplication.presentation.mobilelist.listener.OnMobileItemChangeListener
import com.kittinan.mobileguideapplication.presentation.mobilelist.model.MobileItemViewModel

abstract class MobileListBaseViewModel(
        protected val mobileInteractor: MobileInteractor,
        private val itemChangeListener: OnMobileItemChangeListener? = null,
        private val itemClickListener: (Int) -> Unit
) : BaseViewModel(), LoadDataErrorListener {

    protected var sortType: SortType? = null

    abstract var listType: ListType

    @get:Bindable
    val items = ObservableArrayList<MobileItemViewModel>()

    abstract fun updateDataWithSelectedSortType()

    override fun onRetry() {
        updateDataWithSelectedSortType()
    }

    fun sortItems(sortType: SortType?) {
        this.sortType = sortType
        val sortedItems = getSortedItems(items, sortType)
        items.clear()
        items.addAll(sortedItems)
    }

    protected fun getSortedItems(items: List<MobileItemViewModel>, sortType: SortType?): List<MobileItemViewModel> {
        return when (sortType) {
            SortType.PRICE_ASC -> {
                items.sortedBy { it.mobile.price }
            }
            SortType.PRICE_DESC -> {
                items.sortedByDescending { it.mobile.price }
            }
            SortType.RATING_DESC -> {
                items.sortedByDescending { it.mobile.rating }
            }
            else -> items
        }
    }

    protected fun mapToMobileItemViewModel(mobile: Mobile): MobileItemViewModel {
        return MobileItemViewModel(
                mobileInteractor = mobileInteractor,
                mobile = mobile,
                listType = listType,
                itemChangeListener = itemChangeListener,
                itemClickListener = itemClickListener
        )
    }
}