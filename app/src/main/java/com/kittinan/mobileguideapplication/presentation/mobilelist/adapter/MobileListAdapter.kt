package com.kittinan.mobileguideapplication.presentation.mobilelist.adapter

import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.kittinan.mobileguideapplication.R
import com.kittinan.mobileguideapplication.databinding.ContentMobileFavoriteItemBinding
import com.kittinan.mobileguideapplication.databinding.ContentMobileItemBinding
import com.kittinan.mobileguideapplication.presentation.commons.ListType
import com.kittinan.mobileguideapplication.presentation.commons.ui.RecyclerViewObservableListChangeHandler
import com.kittinan.mobileguideapplication.presentation.mobilelist.model.MobileItemViewModel

class MobileListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val changedHandler by lazy { RecyclerViewObservableListChangeHandler<MobileItemViewModel>(this) }

    var items: ObservableList<MobileItemViewModel> = ObservableArrayList<MobileItemViewModel>()
        set(value) {
            if (field === value) return
            field.removeOnListChangedCallback(changedHandler)
            field = value
            field.addOnListChangedCallback(changedHandler)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.content_mobile_item -> MobileItemViewHolder(
                    ContentMobileItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
            R.layout.content_mobile_favorite_item -> MobileFavoriteItemViewHolder(
                    ContentMobileFavoriteItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        val itemViewModel = items[position]
        return when (itemViewModel.listType) {
            ListType.DEFAULT -> R.layout.content_mobile_item
            ListType.FAVORITE -> R.layout.content_mobile_favorite_item
            else -> super.getItemViewType(position)
        }
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when (holder) {
            is MobileItemViewHolder -> holder.binding.item = item
            is MobileFavoriteItemViewHolder -> holder.binding.item = item
        }
    }

    class MobileItemViewHolder(val binding: ContentMobileItemBinding) : RecyclerView.ViewHolder(binding.root)
    class MobileFavoriteItemViewHolder(val binding: ContentMobileFavoriteItemBinding) : RecyclerView.ViewHolder(binding.root)
}