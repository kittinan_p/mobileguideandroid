package com.kittinan.mobileguideapplication.presentation.mobilelist.adapter

import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.kittinan.mobileguideapplication.R
import com.kittinan.mobileguideapplication.presentation.commons.adapters.setImageUrl

class MobileImagePagerAdapter(private val imageUrls: List<String>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any) = view == (`object` as? ImageView)

    override fun getCount() = imageUrls.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(container.context)
        setImageUrl(
                imageView,
                imageUrls[position],
                ContextCompat.getDrawable(container.context, R.drawable.placeholder)
        )
        (container as? ViewPager)?.addView(imageView)
        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (`object` as? ImageView)?.let {
            (container as? ViewPager)?.removeView(it)
        }
    }
}