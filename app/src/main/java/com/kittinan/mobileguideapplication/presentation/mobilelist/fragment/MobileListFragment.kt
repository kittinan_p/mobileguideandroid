package com.kittinan.mobileguideapplication.presentation.mobilelist.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kittinan.mobileguideapplication.R
import com.kittinan.mobileguideapplication.databinding.FragmentMobileListBinding
import com.kittinan.mobileguideapplication.presentation.commons.string.StringSourceHolder

class MobileListFragment : MobileListBaseFragment<MobileListViewModel>() {

    override val title = StringSourceHolder.getString(R.string.tab_list_text)
    override val recyclerView: RecyclerView by lazy { binding.recyclerView }
    private lateinit var binding: FragmentMobileListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_mobile_list, container, false)
        binding.viewModel = viewModel
        initializeRecyclerView()
        return binding.root
    }

    override fun onCreateViewModel() = MobileListViewModel(
            mobileInteractor = domainComponent.getMobileInteractor(),
            itemChangeListener = itemChangeListener,
            itemClickListener = this::onItemClick
    )
}
