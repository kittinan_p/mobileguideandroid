package com.kittinan.mobileguideapplication.presentation.commons

enum class SortType {
    PRICE_DESC,
    PRICE_ASC,
    RATING_DESC
}