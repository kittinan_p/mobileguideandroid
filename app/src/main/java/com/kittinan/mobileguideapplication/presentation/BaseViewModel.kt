package com.kittinan.mobileguideapplication.presentation

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.util.Log
import com.kittinan.mobileguideapplication.BR
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : BaseObservable() {

    val compositeDisposable by lazy { CompositeDisposable() }

    @get:Bindable
    var loading = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }

    @get:Bindable
    var error = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.error)
        }

    protected fun handleViewOnSubscribe() {
        loading = true
        error = false
    }

    protected fun handleViewAfterTerminate() {
        loading = false
    }

    protected fun handleViewOnError() {
        error = true
    }

    protected fun handleError(throwable: Throwable) {
        Log.e(javaClass.simpleName, throwable.message)
    }
}
