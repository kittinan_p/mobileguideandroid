package com.kittinan.mobileguideapplication.presentation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kittinan.mobileguideapplication.MobileGuideApplication

abstract class BaseFragment<VM : BaseViewModel> : Fragment() {

    protected val domainComponent = MobileGuideApplication.domainComponent
    lateinit var viewModel: VM
    abstract val title: String

    private val onPropertyChangedCallback: android.databinding.Observable.OnPropertyChangedCallback = object : android.databinding.Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: android.databinding.Observable, propertyId: Int) = onViewModelPropertyChanged(propertyId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = onCreateViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel.addOnPropertyChangedCallback(onPropertyChangedCallback)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.compositeDisposable.dispose()
    }

    protected abstract fun onCreateViewModel(): VM
    protected open fun onViewModelPropertyChanged(propertyId: Int) {
        if (propertyId == 0) {
            activity?.invalidateOptionsMenu()
        }
    }

}