package com.kittinan.mobileguideapplication.presentation.commons

enum class ToggleFavorite {
    ADD,
    REMOVE
}