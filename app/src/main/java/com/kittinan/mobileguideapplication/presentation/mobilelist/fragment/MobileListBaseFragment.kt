package com.kittinan.mobileguideapplication.presentation.mobilelist.fragment

import android.content.Context
import android.content.Intent
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.kittinan.mobileguideapplication.presentation.BaseFragment
import com.kittinan.mobileguideapplication.presentation.commons.PageExtra
import com.kittinan.mobileguideapplication.presentation.mobiledetail.MobileDetailActivity
import com.kittinan.mobileguideapplication.presentation.mobilelist.listener.OnMobileItemChangeListener
import com.kittinan.mobileguideapplication.presentation.mobilelist.adapter.MobileListAdapter

abstract class MobileListBaseFragment<VM : MobileListBaseViewModel> : BaseFragment<VM>() {

    abstract val recyclerView: RecyclerView
    protected var itemChangeListener: OnMobileItemChangeListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        itemChangeListener = context as? OnMobileItemChangeListener
    }

    open fun initializeRecyclerView() {
        val adapter = MobileListAdapter()
        adapter.items = viewModel.items

        recyclerView.apply {
            this@apply.adapter = adapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    protected fun onItemClick(id: Int) {
        val intent = Intent(context, MobileDetailActivity::class.java)
        intent.putExtra(PageExtra.MOBILE_ID, id)
        startActivity(intent)
    }
}