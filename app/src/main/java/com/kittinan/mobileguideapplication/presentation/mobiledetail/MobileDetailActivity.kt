package com.kittinan.mobileguideapplication.presentation.mobiledetail

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.MenuItem
import com.kittinan.mobileguideapplication.BR
import com.kittinan.mobileguideapplication.R
import com.kittinan.mobileguideapplication.databinding.ActivityMobileDetailBinding
import com.kittinan.mobileguideapplication.presentation.BaseActivity
import com.kittinan.mobileguideapplication.presentation.commons.PageExtra.MOBILE_ID
import com.kittinan.mobileguideapplication.presentation.mobilelist.adapter.MobileImagePagerAdapter

class MobileDetailActivity : BaseActivity<MobileDetailViewModel>() {

    private val binding: ActivityMobileDetailBinding by lazy {
        DataBindingUtil.setContentView<ActivityMobileDetailBinding>(this, R.layout.activity_mobile_detail)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHomeAsBackButton()
        binding.viewModel = viewModel
    }

    override fun onCreateViewModel(): MobileDetailViewModel = MobileDetailViewModel(
            mobileInteractor = domainComponent.getMobileInteractor(),
            id = intent.extras.getInt(MOBILE_ID, 0)
    )

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setHomeAsBackButton() {
        if (!isTaskRoot) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onViewModelPropertyChanged(propertyId: Int) {
        super.onViewModelPropertyChanged(propertyId)
        if (propertyId == BR.mobileImageUrls) {
            binding.viewPager.adapter = MobileImagePagerAdapter(viewModel.mobileImageUrls)
        }
    }
}