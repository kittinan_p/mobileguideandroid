package com.kittinan.mobileguideapplication.presentation.mobilelist.listener

import com.kittinan.mobileguideapplication.presentation.commons.ListType
import com.kittinan.mobileguideapplication.presentation.commons.ToggleFavorite

interface OnMobileItemChangeListener {
    fun onToggleFavorite(toggleFavorite: ToggleFavorite, listType: ListType? = null)
}