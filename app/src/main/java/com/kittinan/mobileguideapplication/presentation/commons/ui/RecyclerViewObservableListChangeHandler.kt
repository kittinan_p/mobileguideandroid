package com.kittinan.mobileguideapplication.presentation.commons.ui

import android.databinding.ObservableList
import android.support.v7.widget.RecyclerView


class RecyclerViewObservableListChangeHandler<T>(
        private val adapter: RecyclerView.Adapter<*>
) : ObservableList.OnListChangedCallback<ObservableList<T>>() {

    override fun onChanged(list: ObservableList<T>) {
        adapter.notifyDataSetChanged()
    }

    override fun onItemRangeChanged(list: ObservableList<T>, positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeChanged(positionStart, itemCount)
    }

    override fun onItemRangeInserted(list: ObservableList<T>, positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeInserted(positionStart, itemCount)
    }

    override fun onItemRangeMoved(list: ObservableList<T>, fromPosition: Int, toPosition: Int, itemCount: Int) {
        adapter.notifyDataSetChanged()
    }

    override fun onItemRangeRemoved(list: ObservableList<T>, positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeRemoved(positionStart, itemCount)
    }
}
