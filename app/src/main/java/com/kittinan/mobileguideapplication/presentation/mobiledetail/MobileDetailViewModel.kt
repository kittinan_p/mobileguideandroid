package com.kittinan.mobileguideapplication.presentation.mobiledetail

import android.databinding.Bindable
import android.databinding.ObservableArrayList
import com.kittinan.mobileguideapplication.BR
import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.presentation.BaseViewModel
import com.kittinan.mobileguideapplication.presentation.commons.ui.LoadDataErrorListener
import com.kittinan.mobileguideapplication.presentation.mobilelist.model.MobileItemViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MobileDetailViewModel(
        private val mobileInteractor: MobileInteractor,
        private val id: Int
) : BaseViewModel(), LoadDataErrorListener {

    @get:Bindable
    var mobile: MobileItemViewModel? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.mobile)
        }

    @get:Bindable
    val mobileImageUrls = ObservableArrayList<String>()

    init {
        updateData()
    }

    override fun onRetry() {
        updateData()
    }

    private fun updateData() {
        getMobileDetail()
        getMobileImageUrls()
    }

    private fun getMobileDetail() {
        compositeDisposable.add(
                mobileInteractor.getMobileById(id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { mobile = MobileItemViewModel(mobileInteractor, it) },
                                { handleError(it) }
                        )
        )
    }

    private fun getMobileImageUrls() {
        compositeDisposable.add(
                mobileInteractor.getMobileImagesById(id)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { handleViewOnSubscribe() }
                        .doAfterTerminate { handleViewAfterTerminate() }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    mobileImageUrls.addAll(it)
                                    notifyPropertyChanged(BR.mobileImageUrls)
                                },
                                {
                                    handleViewOnError()
                                    handleError(it)
                                }
                        )
        )
    }

}