package com.kittinan.mobileguideapplication.presentation.mobilelist.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.util.SparseArrayCompat
import android.support.v7.app.AppCompatActivity
import com.kittinan.mobileguideapplication.presentation.BaseFragment
import com.kittinan.mobileguideapplication.presentation.commons.ListType
import com.kittinan.mobileguideapplication.presentation.commons.SortType
import com.kittinan.mobileguideapplication.presentation.commons.ToggleFavorite
import com.kittinan.mobileguideapplication.presentation.mobilelist.fragment.MobileFavoriteListFragment
import com.kittinan.mobileguideapplication.presentation.mobilelist.fragment.MobileListBaseFragment
import com.kittinan.mobileguideapplication.presentation.mobilelist.fragment.MobileListFragment

class MobilePagerAdapter(activity: AppCompatActivity) : FragmentStatePagerAdapter(activity.supportFragmentManager) {

    private val fragments = SparseArrayCompat<Fragment>()

    init {
        fragments.put(0, Fragment.instantiate(activity, MobileListFragment::class.java.name))
        fragments.put(1, Fragment.instantiate(activity, MobileFavoriteListFragment::class.java.name))
    }

    override fun getCount(): Int = fragments.size()

    override fun getItem(position: Int): Fragment = fragments.get(position)

    override fun getPageTitle(position: Int): CharSequence? {
        val fragment = fragments.get(position) as BaseFragment<*>
        return fragment.title
    }

    fun sortAllTabs(sortType: SortType) {
        for (i in 0..count) {
            (fragments[i] as? MobileListBaseFragment<*>)?.viewModel?.apply {
                sortItems(sortType)
            }
        }
    }

    fun onToggleFavorite(toggleFavorite: ToggleFavorite, listType: ListType? = null) {
        when (toggleFavorite) {
            ToggleFavorite.ADD -> {
                updateDataOfListType(ListType.FAVORITE)
            }
            ToggleFavorite.REMOVE -> {
                when (listType) {
                    ListType.FAVORITE -> updateDataOfListType(ListType.DEFAULT)
                    ListType.DEFAULT -> updateDataOfListType(ListType.FAVORITE)
                }
            }
        }
    }

    private fun updateDataOfListType(listType: ListType?) {
        when (listType) {
            ListType.DEFAULT -> (fragments[0] as? MobileListFragment)?.viewModel
            ListType.FAVORITE -> (fragments[1] as? MobileFavoriteListFragment)?.viewModel
            else -> null
        }?.updateDataWithSelectedSortType()
    }
}