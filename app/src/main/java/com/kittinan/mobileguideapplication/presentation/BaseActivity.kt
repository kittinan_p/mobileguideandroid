package com.kittinan.mobileguideapplication.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kittinan.mobileguideapplication.MobileGuideApplication

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    protected val domainComponent = MobileGuideApplication.domainComponent
    lateinit var viewModel: VM

    private val onPropertyChangedCallback: android.databinding.Observable.OnPropertyChangedCallback = object : android.databinding.Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: android.databinding.Observable, propertyId: Int) = onViewModelPropertyChanged(propertyId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = onCreateViewModel()
        viewModel.addOnPropertyChangedCallback(onPropertyChangedCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.compositeDisposable.dispose()
    }

    protected abstract fun onCreateViewModel(): VM
    protected open fun onViewModelPropertyChanged(propertyId: Int) {
        if (propertyId == 0) {
            invalidateOptionsMenu()
        }
    }
}