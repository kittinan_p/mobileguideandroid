package com.kittinan.mobileguideapplication.presentation.mobilelist.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kittinan.mobileguideapplication.MobileGuideApplication
import com.kittinan.mobileguideapplication.R
import com.kittinan.mobileguideapplication.databinding.FragmentMobileFavoriteListBinding
import com.kittinan.mobileguideapplication.presentation.commons.string.StringSourceHolder
import com.kittinan.mobileguideapplication.presentation.mobilelist.adapter.MobileListAdapter

class MobileFavoriteListFragment : MobileListBaseFragment<MobileFavoriteListViewModel>() {

    override val title = StringSourceHolder.getString(R.string.tab_favorite_list_text)
    override val recyclerView: RecyclerView by lazy { binding.recyclerView }
    private lateinit var binding: FragmentMobileFavoriteListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_mobile_favorite_list, container, false)
        binding.viewModel = viewModel
        initializeRecyclerView()
        return binding.root
    }

    override fun initializeRecyclerView() {
        super.initializeRecyclerView()
        val simpleItemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder) = false

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                val position = viewHolder.adapterPosition
                (recyclerView.adapter as? MobileListAdapter)?.let {
                    val itemViewModel = it.items[position]
                    itemViewModel.toggleFavorite()
                    it.items.remove(itemViewModel)
                }
            }
        }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onCreateViewModel() = MobileFavoriteListViewModel(
            mobileInteractor = domainComponent.getMobileInteractor(),
            itemChangeListener = itemChangeListener,
            itemClickListener = this::onItemClick
    )
}
