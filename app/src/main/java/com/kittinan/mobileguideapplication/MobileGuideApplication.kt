package com.kittinan.mobileguideapplication

import android.app.Application
import com.kittinan.mobileguideapplication.data.DataComponent
import com.kittinan.mobileguideapplication.domain.DomainComponent
import com.kittinan.mobileguideapplication.presentation.commons.string.ResourceStringSource
import com.kittinan.mobileguideapplication.presentation.commons.string.StringSourceHolder
import io.realm.Realm
import io.realm.RealmConfiguration

class MobileGuideApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        buildDependencyGraph()
        buildRealm()
        setUpComponents()
    }

    private fun buildDependencyGraph() {
        val dataComponent = DataComponent()
        domainComponent = DomainComponent
                .builder()
                .mobileRepository(dataComponent.getMobileRepository())
                .build()
    }

    private fun buildRealm() {
        Realm.init(this)
        val realmConfig = RealmConfiguration.Builder()
                .name("mobile.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(realmConfig)
    }

    private fun setUpComponents() {
        StringSourceHolder.stringSource = ResourceStringSource(resources)
    }

    companion object {
        lateinit var domainComponent: DomainComponent
    }
}