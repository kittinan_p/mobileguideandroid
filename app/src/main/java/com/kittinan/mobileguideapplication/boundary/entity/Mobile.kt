package com.kittinan.mobileguideapplication.boundary.entity

interface Mobile {
    val id: Int
    val name: String
    val description: String
    val brand: String
    val thumbImageURL: String
    val rating: Double
    val price: Double
    var favorite: Boolean?
}