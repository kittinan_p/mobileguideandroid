package com.kittinan.mobileguideapplication.boundary.interactor

import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import io.reactivex.Completable
import io.reactivex.Single

interface MobileInteractor {
    fun getMobiles(): Single<List<Mobile>>
    fun getMobileById(id: Int): Single<Mobile>
    fun getMobileImagesById(id: Int): Single<List<String>>
    fun getFavorites(): Single<List<Mobile>>
    fun updateFavorite(mobile: Mobile): Completable
}