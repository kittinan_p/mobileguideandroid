package com.kittinan.mobileguideapplication.boundary.repository

import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import io.reactivex.Completable
import io.reactivex.Single

interface MobileRepository {
    fun getAll(): Single<List<Mobile>>
    fun getFavorites(): Single<List<Mobile>>
    fun getById(id: Int): Single<Mobile>
    fun getImagesById(id: Int): Single<List<String>>
    fun save(mobile: Mobile): Completable
    fun saveAll(mobiles: List<Mobile>): Completable
    fun clearAll(): Completable
}