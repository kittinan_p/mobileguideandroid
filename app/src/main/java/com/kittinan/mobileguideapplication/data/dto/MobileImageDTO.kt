package com.kittinan.mobileguideapplication.data.dto

data class MobileImageDTO(
        val id: Int,
        val mobile_id: Int,
        val url: String
)