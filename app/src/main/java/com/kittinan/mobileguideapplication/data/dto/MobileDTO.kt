package com.kittinan.mobileguideapplication.data.dto

data class MobileDTO(
        val id: Int? = null,
        val name: String? = null,
        val description: String? = null,
        val brand: String? = null,
        val thumbImageURL: String? = null,
        val rating: Double? = null,
        val price: Double? = null
)