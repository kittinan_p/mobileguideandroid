package com.kittinan.mobileguideapplication.data.repository

import android.util.Log
import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import com.kittinan.mobileguideapplication.boundary.repository.MobileRepository
import com.kittinan.mobileguideapplication.data.mapper.MobileMapper
import com.kittinan.mobileguideapplication.data.mapper.MobileRealmMapper
import com.kittinan.mobileguideapplication.data.realmmodel.MobileRealm
import com.kittinan.mobileguideapplication.data.resource.MobileResource
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class MobileRepositoryImpl(private val mobileResource: MobileResource) : RepositoryBase(), MobileRepository {

    override fun getAll(): Single<List<Mobile>> {
        val localSource = getLocalAll()
        val remoteSource = mobileResource.getMobiles()
                .map { mobileDTOs ->
                    mobileDTOs.map { MobileMapper.mapToEntity(it) }
                }
                .doOnSuccess { mobiles ->
                    saveAll(mobiles)
                            .subscribeOn(Schedulers.io())
                            .subscribeBy(onError = {
                                Log.e("Realm", "saveAll() failed")
                            })
                }

        if (localSource == null || localSource.isEmpty()) return remoteSource
        return Single.fromCallable { localSource }
    }

    override fun getFavorites(): Single<List<Mobile>> {
        return Single.fromCallable {
            readFromRealm { realm ->
                realm.where(MobileRealm::class.java)
                        .equalTo("favorite", true)
                        .findAll()
                        ?.let { results ->
                            results.map { MobileRealmMapper.mapFromRealm(it) }
                        }
            }
        }
    }

    override fun getById(id: Int): Single<Mobile> {
        return Single.fromCallable {
            readFromRealm { realm ->
                realm.where(MobileRealm::class.java)
                        .equalTo("id", id)
                        .findFirst()
                        ?.let {
                            MobileRealmMapper.mapFromRealm(it)
                        }
            }
        }
    }

    override fun getImagesById(id: Int): Single<List<String>> {
        return mobileResource.getMobileImages(id)
                .map { imageDTOs ->
                    imageDTOs.map { MobileMapper.mapToEntity(it) }
                }
    }

    override fun save(mobile: Mobile): Completable {
        return getById(mobile.id)
                .doOnSuccess {
                    try {
                        val existingRealm = it
                        if (mobile.favorite == null && existingRealm.favorite != null) {
                            mobile.favorite = existingRealm.favorite
                        }
                        updateToRealm(MobileRealmMapper.mapToRealm(mobile))
                    } catch (e: Exception) {
                        Log.w("Realm", "Error writing $mobile")
                    }

                }.toCompletable()
    }

    override fun saveAll(mobiles: List<Mobile>): Completable {
        return getByIds(mobiles.map { it.id })
                .doOnSuccess { localMobiles ->
                    try {
                        updateMobileFavoriteStatus(mobiles, localMobiles)
                        updateToRealm(mobiles.map { MobileRealmMapper.mapToRealm(it) })
                    } catch (e: Exception) {
                        Log.w("Realm", "Error writing $mobiles")
                    }
                }.toCompletable()
    }

    override fun clearAll(): Completable {
        return Completable.fromAction {
            try {
                deleteAll<MobileRealm>()
            } catch (e: Exception) {
                Log.w("Realm", "Error clearing MobileRealm")
            }
        }
    }

    private fun updateMobileFavoriteStatus(mobiles: List<Mobile>, localMobiles: List<Mobile>) {
        mobiles.forEach { savingMobile ->
            if (localMobiles.isNotEmpty()) {
                localMobiles.first { it.id == savingMobile.id }.let { localMobile ->
                    if (savingMobile.favorite == null && localMobile.favorite != null) {
                        savingMobile.favorite = localMobile.favorite
                    }
                }
            }
        }
    }

    private fun getByIds(ids: List<Int>): Single<List<Mobile>> {
        return Single.fromCallable {
            readFromRealm { realm ->
                realm.where(MobileRealm::class.java)
                        .`in`("id", ids.toTypedArray())
                        .findAll()
                        ?.let { localMobile ->
                            localMobile.map { MobileRealmMapper.mapFromRealm(it) }
                        }
            }
        }
    }

    private fun getLocalAll(): List<Mobile>? {
        return readFromRealm { realm ->
            realm.where(MobileRealm::class.java)
                    .findAll()
                    ?.let { localMobile ->
                        localMobile.map { MobileRealmMapper.mapFromRealm(it) }
                    }
        }
    }
}