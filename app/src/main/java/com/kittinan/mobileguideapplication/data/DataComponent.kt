package com.kittinan.mobileguideapplication.data

import com.kittinan.mobileguideapplication.data.repository.RepositoryModule
import com.kittinan.mobileguideapplication.data.resource.ResourceModule

class DataComponent {

    private val resourceModule = ResourceModule()
    private val repositoryModule = RepositoryModule(resourceModule.providesMobileResource())

    fun getMobileRepository() = repositoryModule.providesMobileRepository()
}