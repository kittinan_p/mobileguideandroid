package com.kittinan.mobileguideapplication.data.mapper

import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import com.kittinan.mobileguideapplication.data.dto.MobileDTO
import com.kittinan.mobileguideapplication.data.dto.MobileImageDTO
import com.kittinan.mobileguideapplication.data.entity.MobileImpl

object MobileMapper {

    fun mapToEntity(dto: MobileDTO): Mobile {
        return MobileImpl(
                id = dto.id!!,
                name = dto.name ?: "",
                description = dto.description ?: "",
                brand = dto.brand ?: "",
                thumbImageURL = dto.thumbImageURL ?: "",
                rating = dto.rating ?: 0.0,
                price = dto.price ?: 0.0
        )
    }

    fun mapToEntity(dto: MobileImageDTO): String {
        return formatToValidImageUrl(dto.url)
    }

    private fun formatToValidImageUrl(url: String): String {
        if (url.startsWith("https://") || url.startsWith("http://")) {
            return url
        }
        return "http://$url"
    }
}