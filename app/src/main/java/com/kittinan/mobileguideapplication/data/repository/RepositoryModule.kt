package com.kittinan.mobileguideapplication.data.repository

import com.kittinan.mobileguideapplication.data.resource.MobileResource

class RepositoryModule(private val mobileResource: MobileResource) {

    fun providesMobileRepository() = MobileRepositoryImpl(mobileResource)
}