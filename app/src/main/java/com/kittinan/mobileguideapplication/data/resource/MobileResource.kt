package com.kittinan.mobileguideapplication.data.resource

import com.kittinan.mobileguideapplication.data.dto.MobileDTO
import com.kittinan.mobileguideapplication.data.dto.MobileImageDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface MobileResource {

    @GET("mobiles")
    fun getMobiles(): Single<List<MobileDTO>>

    @GET("mobiles/{id}/images")
    fun getMobileImages(@Path("id") id: Int): Single<List<MobileImageDTO>>
}