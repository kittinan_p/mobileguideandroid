package com.kittinan.mobileguideapplication.data.resource

import com.kittinan.mobileguideapplication.data.RetrofitFactory

class ResourceModule {

    fun providesMobileResource(): MobileResource = RetrofitFactory.retrofit.create(MobileResource::class.java)
}