package com.kittinan.mobileguideapplication.data.mapper

import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import com.kittinan.mobileguideapplication.data.entity.MobileImpl
import com.kittinan.mobileguideapplication.data.realmmodel.MobileRealm

object MobileRealmMapper {

    fun mapToRealm(mobile: Mobile): MobileRealm {
        return MobileRealm(
                id = mobile.id,
                name = mobile.name,
                description = mobile.description,
                brand = mobile.brand,
                thumbImageURL = mobile.thumbImageURL,
                rating = mobile.rating,
                price = mobile.price
        ).apply {
            favorite = mobile.favorite
        }
    }

    fun mapFromRealm(mobileRealm: MobileRealm): Mobile {
        return MobileImpl(
                id = mobileRealm.id,
                name = mobileRealm.name!!,
                description = mobileRealm.description!!,
                brand = mobileRealm.brand!!,
                thumbImageURL = mobileRealm.thumbImageURL!!,
                rating = mobileRealm.rating!!,
                price = mobileRealm.price!!,
                favorite = mobileRealm.favorite
        )
    }
}