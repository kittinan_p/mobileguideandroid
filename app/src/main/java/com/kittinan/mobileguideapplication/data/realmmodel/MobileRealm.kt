package com.kittinan.mobileguideapplication.data.realmmodel

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class MobileRealm(
        @PrimaryKey var id: Int = 0,
        var name: String? = null,
        var description: String? = null,
        var brand: String? = null,
        var thumbImageURL: String? = null,
        var rating: Double? = null,
        var price: Double? = null
) : RealmObject() {
    var favorite: Boolean? = null

    override fun toString() = "[id:$id, name:$name, favorite:$favorite]"
}