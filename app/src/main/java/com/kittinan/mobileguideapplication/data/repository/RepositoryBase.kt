package com.kittinan.mobileguideapplication.data.repository

import android.util.Log
import io.realm.Realm
import io.realm.RealmObject

open class RepositoryBase {

    protected val realm: Realm
        get() = Realm.getDefaultInstance()

    protected fun <T> readFromRealm(query: (Realm) -> T?): T? {
        return realm.use { realm ->
            val entity = query(realm)
            Log.v("Realm", "Realm --> $entity")
            entity
        }
    }

    protected fun <T : RealmObject> updateToRealm(realmModels: List<T>) {
        realm.use { realm ->
            realm.executeTransaction {
                realmModels.forEach { realmModel ->
                    realm.insertOrUpdate(realmModel)
                    Log.v("Realm", "Realm <-- $realmModel")
                }
            }
        }
    }

    protected fun <T : RealmObject> updateToRealm(realmModel: T) {
        realm.use { realm ->
            realm.executeTransaction {
                realm.insertOrUpdate(realmModel)
                Log.v("Realm", "Realm <-- $realmModel")
            }
        }
    }

    protected inline fun <reified T : RealmObject> deleteAll() {
        realm.use { realm ->
            realm.executeTransaction {
                val realmModels = realm.where(T::class.java).findAll()
                realmModels.deleteAllFromRealm()
                Log.v("Realm", "Delete all ${T::class.java.simpleName}")
            }
        }
    }
}