package com.kittinan.mobileguideapplication.data.entity

import com.kittinan.mobileguideapplication.boundary.entity.Mobile

class MobileImpl(
        override val id: Int,
        override val name: String,
        override val description: String,
        override val brand: String,
        override val thumbImageURL: String,
        override val rating: Double,
        override val price: Double,
        override var favorite: Boolean? = null
) : Mobile