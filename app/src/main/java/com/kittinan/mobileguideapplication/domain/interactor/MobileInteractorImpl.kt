package com.kittinan.mobileguideapplication.domain.interactor

import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.boundary.repository.MobileRepository
import io.reactivex.Completable
import io.reactivex.Single

class MobileInteractorImpl(private val repository: MobileRepository) : MobileInteractor {

    override fun getMobiles(): Single<List<Mobile>> {
        return repository.getAll()
    }

    override fun getMobileById(id: Int): Single<Mobile> {
        return repository.getById(id)
    }

    override fun getMobileImagesById(id: Int): Single<List<String>> {
        return repository.getImagesById(id)
    }

    override fun getFavorites(): Single<List<Mobile>> {
        return repository.getFavorites()
    }

    override fun updateFavorite(mobile: Mobile): Completable {
        return repository.save(mobile)
    }
}