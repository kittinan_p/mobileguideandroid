package com.kittinan.mobileguideapplication.domain.interactor

import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.boundary.repository.MobileRepository

class InteractorModule(private val repository: MobileRepository) {

    fun providesMobileInteractor(): MobileInteractor = MobileInteractorImpl(repository)
}