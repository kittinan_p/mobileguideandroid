package com.kittinan.mobileguideapplication.domain

import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.boundary.repository.MobileRepository
import com.kittinan.mobileguideapplication.domain.interactor.InteractorModule

class DomainComponent private constructor() {

    private lateinit var mobileRepository: MobileRepository
    private lateinit var interactorModule: InteractorModule

    fun mobileRepository(mobileRepository: MobileRepository) : DomainComponent {
        this.mobileRepository = mobileRepository
        return this
    }

    fun build() : DomainComponent {
        interactorModule = InteractorModule(repository = mobileRepository)
        return this
    }

    // Dependencies provider
    fun getMobileInteractor(): MobileInteractor = interactorModule.providesMobileInteractor()

    companion object {
        fun builder(): DomainComponent = DomainComponent()
    }
}