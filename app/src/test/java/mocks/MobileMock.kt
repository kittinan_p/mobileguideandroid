package mocks

import com.kittinan.mobileguideapplication.boundary.entity.Mobile
import com.kittinan.mobileguideapplication.data.entity.MobileImpl

object MobileMock {

    val mockMobiles = listOf<Mobile>(
            MobileImpl(1, "Mobile 01", "description", "brand", "url", 1.4, 299.9),
            MobileImpl(2, "Mobile 02", "description", "brand", "url", 3.5, 199.9),
            MobileImpl(3, "Mobile 03", "description", "brand", "url", 4.8, 159.9),
            MobileImpl(4, "Mobile 04", "description", "brand", "url", 0.8, 19.9)
    )
    val mockMobileImageUrls = listOf("imageUrl1", "imageUrl2")
    val mockFavoriteMobiles = listOf(mockMobiles[0], mockMobiles[3])
}