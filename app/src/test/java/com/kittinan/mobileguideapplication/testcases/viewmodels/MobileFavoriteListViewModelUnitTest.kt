package com.kittinan.mobileguideapplication.testcases.viewmodels

import com.kittinan.mobileguideapplication.presentation.commons.SortType
import com.kittinan.mobileguideapplication.presentation.mobilelist.fragment.MobileFavoriteListViewModel
import com.kittinan.mobileguideapplication.presentation.mobilelist.listener.OnMobileItemChangeListener
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Single
import mocks.MobileMock.mockFavoriteMobiles
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MobileFavoriteListViewModelUnitTest : BaseUnitTest() {

    private lateinit var sut: MobileFavoriteListViewModel

    @Mock
    private lateinit var itemChangeListener: OnMobileItemChangeListener

    @Before
    override fun before() {
        super.before()

        // given
        whenever(mobileInteractor.getFavorites())
                .thenReturn(Single.just(mockFavoriteMobiles))

        // when
        sut = MobileFavoriteListViewModel(mobileInteractor, itemChangeListener) { }
    }

    @Test
    fun givenFavoriteMobileList_whenInit_thenReturnMobileListCorrectly() {
        // then
        assertEquals(mockFavoriteMobiles, sut.items.map { it.mobile })
    }

    @Test
    fun givenFavoriteMobileList_whenSortItemsByPriceAsc_thenSortListCorrectly() {
        // when
        sut.sortItems(SortType.PRICE_ASC)

        // then
        val expectedList = mockFavoriteMobiles.sortedBy { it.price }
        val actualList = sut.items.map { it.mobile }
        assertEquals(expectedList, actualList)
    }

    @Test
    fun givenFavoriteMobileList_whenSortItemsByPriceDesc_thenSortListCorrectly() {
        // when
        sut.sortItems(SortType.PRICE_DESC)

        // then
        val expectedList = mockFavoriteMobiles.sortedByDescending { it.price }
        val actualList = sut.items.map { it.mobile }
        assertEquals(expectedList, actualList)
    }

    @Test
    fun givenFavoriteMobileList_whenSortItemsByRatingDesc_thenSortListCorrectly() {
        // when
        sut.sortItems(SortType.RATING_DESC)

        // then
        val expectedList = mockFavoriteMobiles.sortedByDescending { it.rating }
        val actualList = sut.items.map { it.mobile }
        assertEquals(expectedList, actualList)
    }

    @Test
    fun givenMobileItem_whenToggleFavorite_thenFavoriteStatusShouldBeToggle() {
        // given
        whenever(mobileInteractor.updateFavorite(any())).thenReturn(Completable.complete())

        // when
        val item = sut.items[0]
        val isFavorite = item.favorite
        item.toggleFavorite()

        // then
        assertEquals(isFavorite.not(), item.favorite)
        verify(itemChangeListener).onToggleFavorite(any(), any())
    }
}