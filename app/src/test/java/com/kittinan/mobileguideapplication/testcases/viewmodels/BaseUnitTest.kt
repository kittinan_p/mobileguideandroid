package com.kittinan.mobileguideapplication.testcases.viewmodels

import com.kittinan.mobileguideapplication.boundary.interactor.MobileInteractor
import com.kittinan.mobileguideapplication.presentation.commons.string.MockStringSource
import com.kittinan.mobileguideapplication.presentation.commons.string.StringSourceHolder
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.mockito.Mock

open class BaseUnitTest {

    @Mock
    protected lateinit var mobileInteractor: MobileInteractor

    @Before
    open fun before() {
        overrideRxSchedulers()
        StringSourceHolder.stringSource = MockStringSource()
    }

    @After
    fun after() {
        unOverrideRxSchedulers()
    }

    private fun overrideRxSchedulers() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    private fun unOverrideRxSchedulers() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}