package com.kittinan.mobileguideapplication.testcases.viewmodels

import com.kittinan.mobileguideapplication.presentation.mobiledetail.MobileDetailViewModel
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import mocks.MobileMock.mockMobileImageUrls
import mocks.MobileMock.mockMobiles
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MobileDetailViewModelUnitTest : BaseUnitTest() {

    private lateinit var sut: MobileDetailViewModel
    private val mobileId = 1

    @Test
    fun givenMobile_whenInit_thenReturnMobileDetail() {
        // given
        val mockMobile = mockMobiles.first { it.id == mobileId }
        whenever(mobileInteractor.getMobileById(mobileId))
                .thenReturn(Single.just(mockMobile))

        whenever(mobileInteractor.getMobileImagesById(mobileId))
                .thenReturn(Single.just(emptyList()))

        // when
        sut = MobileDetailViewModel(mobileInteractor, mobileId)

        // then
        assertEquals(mockMobile, sut.mobile?.mobile)
    }

    @Test
    fun givenMobile_whenInit_thenReturnMobileImageUrls() {
        // given
        val mockMobile = mockMobiles.first { it.id == mobileId }
        whenever(mobileInteractor.getMobileById(mobileId))
                .thenReturn(Single.just(mockMobile))

        whenever(mobileInteractor.getMobileImagesById(mobileId))
                .thenReturn(Single.just(mockMobileImageUrls))

        // when
        sut = MobileDetailViewModel(mobileInteractor, mobileId)

        // then
        assertEquals(mockMobileImageUrls, sut.mobileImageUrls)
    }
}