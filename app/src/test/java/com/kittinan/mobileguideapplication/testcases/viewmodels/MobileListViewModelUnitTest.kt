package com.kittinan.mobileguideapplication.testcases.viewmodels

import com.kittinan.mobileguideapplication.presentation.commons.SortType
import com.kittinan.mobileguideapplication.presentation.mobilelist.fragment.MobileListViewModel
import com.kittinan.mobileguideapplication.presentation.mobilelist.listener.OnMobileItemChangeListener
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Single
import mocks.MobileMock.mockMobiles
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MobileListViewModelUnitTest : BaseUnitTest() {

    private lateinit var sut: MobileListViewModel

    @Mock
    private lateinit var itemChangeListener: OnMobileItemChangeListener

    @Before
    override fun before() {
        super.before()

        // given
        whenever(mobileInteractor.getMobiles())
                .thenReturn(Single.just(mockMobiles))

        // when
        sut = MobileListViewModel(mobileInteractor, itemChangeListener) { }
    }

    @Test
    fun givenMobileList_whenInit_thenReturnMobileListCorrectly() {
        // then
        assertEquals(mockMobiles, sut.items.map { it.mobile })
    }

    @Test
    fun givenMobileList_whenSortItemsByPriceAsc_thenSortListCorrectly() {
        // when
        sut.sortItems(SortType.PRICE_ASC)

        // then
        val expectedList = mockMobiles.sortedBy { it.price }
        val actualList = sut.items.map { it.mobile }
        assertEquals(expectedList, actualList)
    }

    @Test
    fun givenMobileList_whenSortItemsByPriceDesc_thenSortListCorrectly() {
        // when
        sut.sortItems(SortType.PRICE_DESC)

        // then
        val expectedList = mockMobiles.sortedByDescending { it.price }
        val actualList = sut.items.map { it.mobile }
        assertEquals(expectedList, actualList)
    }

    @Test
    fun givenMobileList_whenSortItemsByRatingDesc_thenSortListCorrectly() {
        // when
        sut.sortItems(SortType.RATING_DESC)

        // then
        val expectedList = mockMobiles.sortedByDescending { it.rating }
        val actualList = sut.items.map { it.mobile }
        assertEquals(expectedList, actualList)
    }

    @Test
    fun givenMobileItem_whenToggleFavorite_thenFavoriteStatusShouldBeToggle() {
        // given
        whenever(mobileInteractor.updateFavorite(any())).thenReturn(Completable.complete())

        // when
        val item = sut.items[0]
        val isFavorite = item.favorite
        item.toggleFavorite()

        // then
        assertEquals(isFavorite.not(), item.favorite)
        verify(itemChangeListener).onToggleFavorite(any(), any())
    }
}