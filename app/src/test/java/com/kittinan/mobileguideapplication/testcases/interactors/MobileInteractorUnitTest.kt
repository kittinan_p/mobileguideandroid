package com.kittinan.mobileguideapplication.testcases.interactors

import com.kittinan.mobileguideapplication.boundary.repository.MobileRepository
import com.kittinan.mobileguideapplication.domain.interactor.MobileInteractorImpl
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Single
import mocks.MobileMock.mockMobiles
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MobileInteractorUnitTest {

    @Mock
    private lateinit var mobileRepository: MobileRepository
    private lateinit var sut: MobileInteractorImpl

    @Before
    fun before() {
        sut = MobileInteractorImpl(mobileRepository)
    }

    @Test
    fun givenRepositoryYieldsAllMobiles_whenGetMobiles_thenYieldsThreeMobiles() {
        // given
        whenever(mobileRepository.getAll())
                .thenReturn(Single.just(mockMobiles))

        // when
        val testObserver = sut.getMobiles().test()

        // then
        testObserver.assertResult(mockMobiles)
    }

    @Test
    fun givenRepositoryYieldsEmptyMobile_whenGetMobiles_thenYieldsEmptyMobile() {
        // given
        whenever(mobileRepository.getAll())
                .thenReturn(Single.just(emptyList()))

        // when
        val testObserver = sut.getMobiles().test()

        // then
        testObserver.assertResult(emptyList())
    }

    @Test
    fun givenRepositoryYieldsMobile_whenGetMobileById_thenYieldsMobile() {
        // given
        whenever(mobileRepository.getById(1))
                .thenReturn(Single.just(mockMobiles[0]))

        // when
        val testObserver = sut.getMobileById(1).test()

        // then
        testObserver.assertResult(mockMobiles[0])
    }

    @Test
    fun givenRepositoryYieldsAllFavoriteMobiles_whenGetFavorites_thenYieldsTwoFavoriteMobiles() {
        // given
        whenever(mobileRepository.getAll())
                .thenReturn(Single.just(listOf(mockMobiles[0], mockMobiles[2])))

        // when
        val testObserver = sut.getMobiles().test()

        // then
        testObserver.assertResult(listOf(mockMobiles[0], mockMobiles[2]))
    }

    @Test
    fun givenRepositoryYieldsEmptyFavoriteMobile_whenGetFavorites_thenYieldsEmptyFavoriteMobile() {
        // given
        whenever(mobileRepository.getAll())
                .thenReturn(Single.just(emptyList()))

        // when
        val testObserver = sut.getMobiles().test()

        // then
        testObserver.assertResult(emptyList())
    }

    @Test
    fun givenRepositorySaveComplete_whenUpdateFavoriteMobile_thenUpdateComplete() {
        // given
        whenever(mobileRepository.save(mockMobiles[1]))
                .thenReturn(Completable.complete())

        // when
        val testObserver = sut.updateFavorite(mockMobiles[1]).test()

        // then
        testObserver.assertComplete()
    }
}